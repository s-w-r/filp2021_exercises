# Выполнение практических работ
Для выполнения практики нужно создать ветку с ее названием, например для первой работы будет ветка exercises-01

в интерфейсе в меню Git-New branch

или в терминале

    git checkout -b exercises-01

Далее идет работа с кодом, после ее выполнения нужно отправить изменения в гитлаб

в интерфейсе

![Commit](images/commit.png)

или в терминале 

    git add .
    git commit -m "Иванов Иван Exercises-01"
    git push origin exercises-01

После этого нужно поставить Merge Request

Обратите внимание, что нужно ставить Target Branch в свой репозиторий, а не общий!

![Create Merge Request](images/merge-request.png)

Если тесты не прошли, то возможности смержить ветку с master не будет, так же в интерфейсе можно будет увидеть это

![Failed Merge Request](images/merge-request-fail.png)

После исправления тестов статус пайплайна поменяется на Success

![Succeed Merge Request](images/merge-request-succeed.png)

# Синхронизация с основным репозиторием
Для того чтобы подтягивать в свой репозиторий изменения из основного, его нужно добавить в качестве нового remote

    git remote add upstream https://gitlab.com/filp2021/filp2021_exercises.git

После этого можно получить изменения

    git pull upstream master

И отправить их к себе в репозиторий

    git push origin master